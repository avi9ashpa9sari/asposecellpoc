﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aspose.Cells;
using System.Data;
using System.Data.SqlClient;

namespace AsposeCellsPoc
{
    public class Class1
    {

        public DataTable BindData()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Header1", typeof(string)));
            dt.Columns.Add(new DataColumn("Table Header 2", typeof(string)));
            dt.Columns.Add(new DataColumn("Table Header 3", typeof(string)));
            dt.Columns.Add(new DataColumn("Table Header 4", typeof(string)));

            DataRow rowData = dt.NewRow();
            rowData["Table Header 1"] = "1";
            rowData["Table Header 2"] = "2";
            rowData["Table Header 3"] = "3";
            rowData["Table Header 4"] = "4";
            dt.Rows.Add(rowData); // Add a new row of data
            return dt;
        }

        public static bool ExportExcelWithAspose(System.Data.DataTable data, string filepath)
        {
            try
            {
                if (data == null)
                {
                    Console.WriteLine("Data is empty");
                    return false;
                }
                Aspose.Cells.License li = new Aspose.Cells.License();
                li.SetLicense("ASPOSE/License.lic"); //Cracking Certificate

                Workbook book = new Workbook(); // Create A Workbook
                Worksheet sheet = book.Worksheets[0]; // Create a worksheet
                Cells cells = sheet.Cells; // Cells
                                           // Create Styles
                Aspose.Cells.Style style = book.DefaultStyle;
                style.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle = Aspose.Cells.CellBorderType.Thin; // Apply the left boundary of the boundary line 
                style.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin; // Apply the right boundary of the boundary line 
                style.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle = Aspose.Cells.CellBorderType.Thin; // Apply the boundary line on the boundary line 
                style.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle = Aspose.Cells.CellBorderType.Thin; // Apply the underlying boundary 
                style.HorizontalAlignment = TextAlignmentType.Center; // Cell content is horizontally aligned with text in the middle
                //style.Font.Name = song Style;//Font.Name
                                             // style1. Font. IsBold = true; // Set bold
                style.Font.Size = 11; // Set font size
                                      // Style. ForegroundColor = System. Drawing. Color. FromArgb (153, 204, 0); //Background color
                                      //style.Pattern = Aspose.Cells.BackgroundType.Solid; 

                int Colnum = data.Columns.Count; // Table column number 
                int Rownum = data.Rows.Count; // Table row count 
                                              // Generate row and column name rows 
                for (int i = 0; i < Colnum; i++)
                {
                    cells[0, i].PutValue(data.Columns[i].ColumnName); // Add table headers
                    cells[0, i].SetStyle(style); // Add style
                }
                // Generate data rows 
                for (int i = 0; i < Rownum; i++)
                {
                    for (int k = 0; k < Colnum; k++)
                    {
                        cells[1 + i, k].PutValue(data.Rows[i][k].ToString()); //Add data
                        cells[1 + i, k].SetStyle(style); // Add style
                    }
                }
                sheet.AutoFitColumns(); // Adaptive Width
                book.Save(filepath); //save
                Console.WriteLine("Excel successfully saved to D disk!!! ");
            
                GC.Collect();
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

    }
}