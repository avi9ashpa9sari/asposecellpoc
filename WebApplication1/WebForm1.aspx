﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <table>
                <tr>
                    <td>
                        <asp:Label runat="server" ID="lblAspose" Text="choose the report"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlAspose" DataTextField="col_name" DataValueField="col_val" runat="server">
                        <asp:ListItem Value="0">--select the YTD report--</asp:ListItem>
                            <asp:ListItem Value="1">Group1</asp:ListItem>
                            <asp:ListItem Value="2">Group2</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        
                        <asp:Button ID="btnGenerate" runat="server" Font-Bold="True" OnClick="btnGenerate_Click" Text="Generate" Width="95px" />
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        
                        &nbsp;</td>
                </tr>
            </table>
        </div>
    </form>
    <p>
        &nbsp;</p>
</body>
</html>
