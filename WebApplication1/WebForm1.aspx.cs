﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Cells;
using Aspose.Cells.GridWeb;
using System.Data;
using System.Collections;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        GridWeb GridWeb1 = new GridWeb();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Uncomment the code below when you have purchased license
                // for Aspose.Cells.GridWeb, Aspose.Chart and Aspose.Cells. You need
                // to deploy the licenses in the same folder as your executable,
                // alternatively you can add the license files as an embedded
                // resource to your project.
                //
                // Set the license for Aspose.Cells.GridWeb
                // Aspose.Cells.GridWeb.License gridwebLicense = new
                // Aspose.Cells.GridWeb.License();
                // gridwebLicense.SetLicense("Aspose.Grid.lic");
                //
                // // Set the license for Aspose.Chart
                // Aspose.Chart.License chartLicense = new
                // Aspose.Chart.License();
                // chartLicense.SetLicense("Aspose.Chart.lic");
                //
                // // Set the license for Aspose.Cells
                // Aspose.Cells.License cellsLicense = new
                // Aspose.Cells.License();
                // cellsLicense.SetLicense("Aspose.Cells.lic");

                //Create a DataSet object.
                DataSet ds = new DataSet();
                
                //Get the Virtual Folder Path.
                string path = MapPath(".");
                //Reads XML data from xml file into DataSet object.
                ds.ReadXml(path + "\\file\\Products.xml");
                //Call the custom method to obtain distinct values from
                //CategoryName field and store data into an object array.
                object[] drs = GetDistinctValues(ds.Tables[0], "CategoryName");
                //Fill the drop down list with distinct field items.
                for (int i = 0; i < drs.Length; i++)
                {
                    ddlAspose.Items.Add(drs[i].ToString());
                }
            }
        }
        private object[] GetDistinctValues(DataTable dtable, string colName)
        {
            // Create a Hashtable object.
            Hashtable hTable = new Hashtable();
            // Loop through the datatable rows and add distinct values to
            // Hashtable object minimizing the duplicates in the field.
            foreach (DataRow drow in dtable.Rows)
                if (!hTable.ContainsKey(drow[colName]))
                    hTable.Add(drow[colName], string.Empty);
            // Create an object array based on the distinct key values of the Hashtable object.
            object[] objArray = new object[hTable.Keys.Count];
            // Copy the disctinct values to fill the array.
            hTable.Keys.CopyTo(objArray, 0);
            // Return the array object.
            return objArray;
        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {

            //Clears datasheets of the GridWeb control.
            GridWeb1.WebWorksheets.Clear();

            //Create a DataSet object.
            DataSet ds = new DataSet();

            //Get the Virtual Folder path.
            string path = MapPath(".");

            //Reads XML data from xml file into DataSet object.
            ds.ReadXml(path + "\\file\\Products.xml");

            //Create a DataView based on the datatable.
            DataView dv = new DataView(ds.Tables[0]);

            //Filter data in the DataView object based on the selected drop down list item.
            dv.RowFilter = "CategoryName ='" + ddlAspose.SelectedItem.Text + "'";

            //Importing data from the filtered DataView object to create and
            //fill "Products" Worksheet start from A4 cell.
            GridWeb1.WebWorksheets.ImportDataView(dv, null, null, "Products", 3, 0);
        }
    }
}