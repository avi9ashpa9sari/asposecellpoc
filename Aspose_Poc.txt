Import data from a datatable into a worksheet using Aspose.Cells APIs.
Create simple WinForm application,
create an excel file and import data table to the worksheet in the workbook.

steps:
1. Instantiate a Workbook object.
2. Obtain the reference of Worksheet
3. Add value to "A1" cell
4. Instantiate a "Products" DataTable object
5. Add columns to the DataTable object
6. Create an empty row in the DataTable object
7. Add data to the row
8. Add filled row to the DataTable object
9. Repeat step 6 to 8 for another row
10. Import the contents of DataTable to the worksheet start from "A3" cell.
11. Auto-Fit all the columns in the worksheet
12. Save the Excel file in xlsx format

 