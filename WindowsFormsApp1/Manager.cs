﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApp1
{
    public class Manager
    {
        private int _mgrId;
        private string _mgrName;

        public Manager(int mgrId, string mgrName)
        {
            this._mgrId = mgrId;
            this._mgrName = mgrName;
        }

        public int MgrId { get; set; }
        public string MgrName { get; set; }
    }
}
