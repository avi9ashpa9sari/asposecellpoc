﻿using Aspose.Cells;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace WindowsFormsApp1
{
    public class UsingGenericList
    {
        public class Employee
        {
            private String emp_Name;

            public String empName
            {
                get { return emp_Name; }
                set { emp_Name = value; }
            }

            private int emp_Id;

            public int empId
            {
                get { return emp_Id; }
                set { emp_Id = value; }
            }

            internal Employee(string empname, int empid)
            {
                this.empName = empname;
                this.empId = empid;
            }

            // Accepting a generic List as a Nested Object
            private List<Manager> emp_Mgr;

            public List<Manager> Manager
            {
                get { return emp_Mgr; }
                set { emp_Mgr = value; }
            }

        }

        public class Manager
        {
            public Manager(string mgrname, int mgrid)
            {
                this.mgr_name = mgrname;
                this.mgr_Id = mgrid;
            }

            private string mgr_name;

            public string mgrName
            {
                get { return mgr_name; }
                set { mgr_name = value; }
            }
            private int mgr_Id;

            public int mgrId
            {
                get { return mgr_Id; }
                set { mgr_Id = value; }
            }
        }

        public static void BindHWUsingGenericList()
        {
            // ExStart:1
            // The path to the documents directory.
            string dataDir = "C:/Users/Administrator/Documents/Visual Studio 2017/Projects/AsposCellsPoc/";
            Workbook workbook = new Workbook();

            // Create a designer workbook

            // Workbook workbook = new Workbook();
            Worksheet worksheet = workbook.Worksheets[0];

            worksheet.Cells["A9"].PutValue("Employee Name");
            worksheet.Cells["A10"].PutValue("&=Employee.empName");

            worksheet.Cells["B9"].PutValue("Employee Id");
            worksheet.Cells["B10"].PutValue("&=Employee.empId");

            worksheet.Cells["C9"].PutValue("Manager's Name");
            worksheet.Cells["C10"].PutValue("&=Employee.Manager.mgrName");

            worksheet.Cells["D9"].PutValue("Manager's Id");
            worksheet.Cells["D10"].PutValue("&=Employee.Manager.mgrId");

            // Apply Style to A1:D1
            Range range = worksheet.Cells.CreateRange("A9:D9");
            Style style = workbook.CreateStyle();
            style.Font.IsBold = true;
            style.ForegroundColor = Color.Yellow;
            style.Pattern = BackgroundType.Solid;
            StyleFlag flag = new StyleFlag();
            flag.All = true;
            range.ApplyStyle(style, flag);

            // Initialize WorkbookDesigner object
            WorkbookDesigner designer = new WorkbookDesigner();

            // Load the template file
            designer.Workbook = workbook;

            System.Collections.Generic.List<Employee> list = new System.Collections.Generic.List<Employee>();

            // Create an object for the Employee class
            Employee h1 = new Employee("Mark John", 30);

            // Create the relevant Wife objects for the Employee object
            h1.Manager = new List<Manager>();
            h1.Manager.Add(new Manager("Chen Zhao", 34));
            h1.Manager.Add(new Manager("Jamima Winfrey", 28));
            h1.Manager.Add(new Manager("Reham Smith", 35));

            // Create another object for the Employee class
            Employee h2 = new Employee("Masood Shankar", 40);

            // Create the relevant Wife objects for the Employee object
            h2.Manager = new List<Manager>();
            h2.Manager.Add(new Manager("Karishma Jathool", 36));
            h2.Manager.Add(new Manager("Angela Rose", 33));
            h2.Manager.Add(new Manager("Hina Khanna", 45));

            // Add the objects to the list
            list.Add(h1);
            list.Add(h2);

            // Specify the DataSource
            designer.SetDataSource("Employee", list);

            // Process the markers
            designer.Process();

            // Autofit columns
            worksheet.AutoFitColumns();

            // Save the Excel file.
            designer.Workbook.Save(dataDir + "output3.xlsx");

            // ExEnd:1
        }
    }
}
