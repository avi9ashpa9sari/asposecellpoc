﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApp1
{
    public class Employee
    {
        private int _empId;
        private string _empName;

        public Employee(int EmpId, string EmpName)
        {
            this._empId = EmpId;
            this._empName = EmpName;
        }

        public int EmpId { get; set; }
        public string EmpName { get; set; }

        private List<Manager> m_Mgr;

        public List<Manager> Manager
        {
            get { return m_Mgr; }
            set { m_Mgr = value; }
        }
    }
}
