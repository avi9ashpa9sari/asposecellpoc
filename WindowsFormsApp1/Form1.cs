﻿using Spire.Xls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Aspose.Cells;
using Aspose.Cells.GridWeb;
using System.IO;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_click(object sender, EventArgs e)
        {


        }

        private void button1_Click(object sender, EventArgs e)
        {
            //select* from [dbo].[Student_Information]            


        }

        private void button5_Click(object sender, EventArgs e)
        {
            SqlConnection Con = new SqlConnection("Data Source=DOTNET;Initial Catalog=TaskManager;User ID=sa;Password=***********;Integrated Security=true");
            Con.Open();
            SqlDataAdapter Da = new SqlDataAdapter(txtQuery.Text, Con);
            DataTable Tab = new DataTable();
            Da.Fill(Tab);
            dataGridView2.DataSource = Tab;
            Con.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string fileName;
            Spire.DataExport.XLS.CellExport cellExport = new Spire.DataExport.XLS.CellExport();
            Spire.DataExport.XLS.WorkSheet worksheet1 = new Spire.DataExport.XLS.WorkSheet();
            worksheet1.DataSource = Spire.DataExport.Common.ExportSource.DataTable;
            worksheet1.DataTable = this.dataGridView2.DataSource as DataTable;
            worksheet1.StartDataCol = ((System.Byte)(0));
            cellExport.Sheets.Add(worksheet1);
            cellExport.ActionAfterExport = Spire.DataExport.Common.ActionType.OpenView;
            fileName = txtFileName.Text.ToString() + ".xlsx";
            cellExport.SaveToFile(fileName);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            string fileName;
            fileName = txtExcelToDatabase.Text.ToString();
            Spire.Xls.Workbook workbook = new Spire.Xls.Workbook();
            workbook.LoadFromFile(fileName);
            Spire.Xls.Worksheet sheet = workbook.Worksheets[0];
            this.dataGridView2.DataSource = sheet.ExportDataTable();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            SqlConnection Con = new SqlConnection("Data Source=DOTNET;Initial Catalog=TaskManager;User ID=sa;Password=***********;Integrated Security=true");
            SqlCommand com;
            string str;
            Con.Open();
            for (int index = 0; index < dataGridView2.Rows.Count - 1; index++)
            {
                str = @"Insert Into Employee(Employee_Id, Employee_Name, Manager_Id,Project_Id) Values(" + dataGridView2.Rows[index].Cells[0].Value.ToString() + ", '" + dataGridView2.Rows[index].Cells[1].Value.ToString() + "'," + dataGridView2.Rows[index].Cells[2].Value.ToString() + "," + dataGridView2.Rows[index].Cells[3].Value.ToString() + ")";
                com = new SqlCommand(str, Con);
                com.ExecuteNonQuery();
            }
            Con.Close();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
            Aspose.Cells.Worksheet worksheet = workbook.Worksheets[0];
            worksheet.Cells["A1"].PutValue("Import a data table");

            DataTable dtManager = new DataTable("Manager");

            dtManager.Columns.Add("Manager Id", typeof(int));
            dtManager.Columns.Add("Manager Name", typeof(string));
            dtManager.Columns.Add("Units", typeof(string));

            DataRow drow = dtManager.NewRow();

            drow[0] = 1;
            drow[1] = "Mobile";
            drow[2] = 15;

            dtManager.Rows.Add(drow);

            drow = dtManager.NewRow();
            drow[0] = 2;
            drow[1] = "Laptop";
            drow[2] = 10;

            dtManager.Rows.Add(drow);

            drow = dtManager.NewRow();
            drow[0] = 3;
            drow[1] = "EarPhone";
            drow[2] = 20;

            dtManager.Rows.Add(drow);

            worksheet.Cells.ImportDataTable(dtManager, true, "A2");

            worksheet.AutoFitColumns();
            workbook.Save("Output1.xlsx", SaveFormat.Xlsx);
            this.dataGridView2.DataSource = worksheet.Cells.ExportDataTable(0, 0, 3, 3); //comment this, not required

        }

        private void btnFormula_Click(object sender, EventArgs e)
        {
            string dataDir = "C:/Users/Administrator/Documents/Visual Studio 2017/Projects/AsposCellsPoc/";              //txtExcelToDatabase.Text.ToString();

            Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();

            // Create a designer workbook

            // Workbook workbook = new Workbook();
            Aspose.Cells.Worksheet worksheet = workbook.Worksheets[0];

            worksheet.Cells["A1"].PutValue("Employee Id");
            worksheet.Cells["A2"].PutValue("&=Employee.EmpId");

            worksheet.Cells["B1"].PutValue("Employee Name");
            worksheet.Cells["B2"].PutValue("&=Employee.EmpName");

            worksheet.Cells["C1"].PutValue("Manager's Id");
            worksheet.Cells["C2"].PutValue("&=Manager.MgrId");

            worksheet.Cells["D1"].PutValue("Manager's Name");
            worksheet.Cells["D2"].PutValue("&=Manager.MgrName");

            // Apply Style to A1:D1
            Range range = worksheet.Cells.CreateRange("A1:D1");
            Style style = workbook.CreateStyle();
            style.Font.IsBold = true;
            style.ForegroundColor = Color.Yellow;
            style.Pattern = BackgroundType.Solid;
            StyleFlag flag = new StyleFlag();
            flag.All = true;
            range.ApplyStyle(style, flag);

            // Initialize WorkbookDesigner object
            WorkbookDesigner designer = new WorkbookDesigner();

            // Load the template file
            designer.Workbook = workbook;

            System.Collections.Generic.List<Employee> list = new System.Collections.Generic.List<Employee>();

            // Create an object for the Employee class
            Employee h1 = new Employee(30, "Mark John");

            // Create the relevant Manager objects for the Employee object
            h1.Manager = new List<Manager>();
            h1.Manager.Add(new Manager(34, "Chen Zhao"));
            h1.Manager.Add(new Manager(28, "Jamima Winfrey"));
            h1.Manager.Add(new Manager(35, "Reham Smith"));

            // Create another object for the Employee class
            Employee h2 = new Employee(40, "Masood Shankar");

            // Create the relevant Manager objects for the Employee object
            h2.Manager = new List<Manager>();
            h2.Manager.Add(new Manager(36, "Karishma Jathool"));
            h2.Manager.Add(new Manager(33, "Angela Rose"));
            h2.Manager.Add(new Manager(45, "Hina Khanna"));

            // Add the objects to the list
            list.Add(h1);
            list.Add(h2);

            // Specify the DataSource
            designer.SetDataSource("Employee", list);

            // Process the markers
            designer.Process();

            // Autofit columns
            worksheet.AutoFitColumns();

            // Save the Excel file.
            designer.Workbook.Save(dataDir + "output2.xls");

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        public DataSet LoadEmployeeData()
        {
            SqlConnection Con = new SqlConnection("Data Source=DOTNET;Initial Catalog=TaskManager;User ID=sa;Password=***********;Integrated Security=true");
            SqlCommand com;
            string str;
            Con.Open();
            str = @"select Employee_Id, Employee_Name, Manager_Id, Project_Id from Employee";
            com = new SqlCommand(str, Con);            
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = com;
            DataSet ds = new DataSet();
            da.Fill(ds,"Employee");
            Con.Close();
            return ds;
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            UsingGenericList.BindHWUsingGenericList();        //Working for Hb-wf

            LoadEmployeeData();

            string dataDir = "C:/Users/Administrator/Documents/Visual Studio 2017/Projects/AsposCellsPoc/";

            // Create directory if it is not already present.
            bool IsExists = System.IO.Directory.Exists(dataDir);
            if (!IsExists)
                System.IO.Directory.CreateDirectory(dataDir);

            if (designerFile != null)
            {

                // Instantiating a WorkbookDesigner object
                WorkbookDesigner designer = new WorkbookDesigner();

                // Open a designer spreadsheet containing smart markers
                designer.Workbook = new Aspose.Cells.Workbook(designerFile);

                // Set the data source for the designer spreadsheet
                designer.SetDataSource(dataset);

                // Process the smart markers
                designer.Process();
            }
        }

        public static Stream designerFile { get; set; }

        public static System.Data.SqlClient.SqlConnection dataset { get; set; }
    }
}
